

var ConstrDetailsApp = angular.module('constructionDetailsApp', ['angularUtils.directives.dirPagination']);
var usersCtrl=ConstrDetailsApp.controller('constructionDetailsCtrl', function($scope, $http) {
	
	$scope.init = function(constructionId)
	  {
		$scope.taskTimekeeping = {};
	    $http.get('/api/constructions/' +constructionId)
	    .then(function (response) {
	    	$scope.construction = response.data;
	    	 angular.forEach($scope.construction.tasks, function(task, index) {
	 	    	
	 	    	$http.get('/api/tasks/' + task.id + '/timekeeping')
	 		    .then(function (response) {
	 		    	$scope.taskTimekeeping[task.id] = response.data;
	 		    });
	 	    	
	 	    });
	    });
	  };
	  
		$scope.createTask = function() {
			// Defining $http service for creating a person
			$scope.newTask.construction=$scope.construction;
			$http({
				method : 'POST',
				url : '/api/tasks/',
				data : JSON.stringify($scope.newTask),
				headers : {
					'Content-Type' : 'application/JSON'
				}
			}).success(function(data) {
				// Showing Success message
				$scope.construction.tasks.push(angular.fromJson(data));
				// Loading people to the $scope

			}).error(
					function(error) {
						alert('Could not create construction')
						$scope.status = 'Unable to create a construction: '
								+ error.message;
					});
		}
		
		$scope.getTaskTimekeeping=function(taskId) {
			var timekeeping=[];
			var taskKeepingUrl='/api/tasks/' +taskId +'/timekeeping';
			$http.get(taskKeepingUrl)
		    .then(function (response) {
		    	timekeeping = response.data;
		    });
			return timekeeping;
		}
		
		$scope.getConstructionTimekeeping=function(constructionId) {
			var constructionTimekeeping=[];
			var constructionTimekeepingUrl='/api/constructions/' +constructionId +'/timekeeping';
			$http.get(constructionTimekeepingUrl)
		    .then(function (response) {
		    	constructionTimekeeping = response.data;
		    });
			return constructionTimekeeping;
		}
	
	 $scope.showCreateTask = function() {
		$('div#task-create').toggle('slow');
	}
});

/**
 * Owner service
 */
var MaterialsService = ConstrDetailsApp.factory('MaterialsService', ['$http', '$q', function($http, $q) {
	return {
			getAllByConstruction : function(constructionId) {
				return $http.get('/api/' + constructionId +'/materials').then(function(response) {
					return response.data;
				}, function(errResponse) {
					console.error('Error fetching constructions');
					return $q.reject(errResponse);
				});
			},
			
			getAllUnitMeasures : function() {
				return $http.get('/api/unitmeasures').then(function(response) {
					return response.data;
				}, function(errResponse) {
					console.error('Error fetching constructions');
					return $q.reject(errResponse);
				});
			},
			
			create : function(material) {
				return $http.post('/api/materials', material).then(
						function(response) {
							return response.data;
						}, function(errResponse) {
							console.error('Error while creating user');
							return $q.reject(errResponse);
						});
			}
		}
	} 
]);

/**
 * Owner service
 */
var ConstrDetailsService = ConstrDetailsApp.factory('ConstrDetailsService', ['$http', '$q', function($http, $q) {
	return {
			findById : function(constructionId) {
				return $http.get('/api/constructions/' +constructionId).then(function(response) {
					return response.data;
				}, function(errResponse) {
					console.error('Error fetching constructions');
					return $q.reject(errResponse);
				});
			}
		}
	} 
]);
			

/**
 * Constructions controller
 */
var ConstrDetailsCtrl = ConstrDetailsApp.controller('ConstrDetailsCtrl', ['$scope','MaterialsService', 'ConstrDetailsService', '$http',
		function($scope, MaterialsService, ConstrDetailsService, $http) {
			var self = this;
			self.construction={};
			self.newMaterial={};
			self.materials=[];
			owners=[];
			unitmeasures=[];
			
			$scope.init = function(constructionId)
		  {
				ConstrDetailsService.findById(constructionId).then(
						function(data) {
							self.construction=data;
							self.loadAllMaterials();
						}, function(errResponse) {
							console.error('Error loading construction.');
					});
		  };
			
		  self.calculateMaterialsCost = function(materialCollections) {
				var sum=0;
				angular.forEach(materialCollections, function(material, key) {
				  sum+=material.totalPrice;
				});
				return sum;
			}
			self.createMaterial = function() {
				self.newMaterial.construction=self.construction;
				MaterialsService.create(self.newMaterial).then(
						function(reseponse) {
							self.materials.push(reseponse);
							self.newMaterial={};
							alert('Success');
						}, function(errResponse) {
							console.error('Error while creating User.');
					});
			};
			
			self.loadAllUMs = function() {
				MaterialsService.getAllUnitMeasures().then(function(data) {
					self.unitmeasures = data;
				}, function(errResponse) {
					console.error('Could not initialize owner list');
				});
			};
			
			self.loadAllMaterials = function() {
				MaterialsService.getAllByConstruction(self.construction.id).then(function(data) {
					self.materials = data;
				}, function(errResponse) {
					console.error('Could not initialize owner list');
				});
			};
			
			self.loadAllUMs();
	}
])
