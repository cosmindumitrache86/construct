var ConstrDetailsApp = angular.module('constrDetailsApp', [ ]);
/**
 * Owner service
 */
var MaterialsService = ConstrDetailsApp.factory('MaterialsService', ['$http', '$q', function($http, $q) {
	return {
			getAllByConstruction : function(consructin) {
				return $http.get('/api/materials').then(function(response) {
					return response.data;
				}, function(errResponse) {
					console.error('Error fetching constructions');
					return $q.reject(errResponse);
				});
			},
			
			create : function(material) {
				return $http.post('/api/materials', material).then(
						function(response) {
							return response.data;
						}, function(errResponse) {
							console.error('Error while creating user');
							return $q.reject(errResponse);
						});
			}
		}
	} 
]);

/**
 * Constructions controller
 */
var ConstrDetailsCtrl = ConstrDetailsApp.controller('ConstrDetailsCtrl', ['$scope','MaterialsService','$http',
		function($scope, MaterialsService, $http) {
			var self = this;
			owners=[];
			self.newMaterial.name='Test';
			
			self.createMaterial = function() {
				MaterialsService.create(self.newMaterial).then(
						function(reseponse) {
							alert('success');
						}, function(errResponse) {
							console.error('Error while creating User.');
					});
			};
	}
])