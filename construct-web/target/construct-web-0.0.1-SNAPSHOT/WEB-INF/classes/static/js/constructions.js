var app = angular.module('constructionsApp', []);
var constructionsCtrl = app.controller('constructionsCtrl', function($scope, $http) {
	$http.get("/api/constructions").then(function(response) {
		$scope.constructions = response.data;
	});

	$scope.createConstruction = function() {
		// Defining $http service for creating a person
		$http({
			method : 'POST',
			url : '/api/constructions/',
			data : JSON.stringify($scope.newConstruction),
			headers : {
				'Content-Type' : 'application/JSON'
			}
		}).success(function(data) {
			// Showing Success message
			$scope.constructions.push(angular.fromJson(data));
			// Loading people to the $scope

		}).error(
				function(error) {
					alert('Could not create construction')
					$scope.status = 'Unable to create a construction: '
							+ error.message;
				});
	}

	$scope.showCreate = function() {
		$('div#construction-create').toggle('slow');
	}
});
