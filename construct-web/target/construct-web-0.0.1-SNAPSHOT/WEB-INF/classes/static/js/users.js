var app = angular.module('usersApp', []);
var usersCtrl=app.controller('usersCtrl', function($scope, $http) {
	$scope.editableUserId=-1;
    $http.get("/api/users")
    .then(function (response) {
    	$scope.users = response.data;
    });
    
    $scope.DeleteUser = function (userId) {
        $http.delete('/api/users' + userId)
              .success(function (data) {
            	  
            	 var usersTemp = eval( $scope.users );
          		for( var i = 0; i < usersTemp.length; i++ ) {
          			if( usersTemp[i].id === userId ) {
          				index = i;
          				break;
          			}
          		}
          		if( index === -1 ) {
          			alert( "Something gone wrong" );
          		}
          		$scope.users.splice( index, 1 );	
                 // either this
                 // $scope.refresh();
                 // or this
                 // $scope.users.splice(id,1);
              });
    }
    
    $scope.createPerson = function () {
        // Defining $http service for creating a person
        $http({
            method: 'POST',
            url: '/api/users/',
            data: JSON.stringify($scope.newPerson),
            headers: { 'Content-Type': 'application/JSON' }
        }).
        success(function (data) {
            // Showing Success message
        	$scope.users.push(angular.fromJson(data));
            // Loading people to the $scope

        })
        .error(function (error) {
            // Showing error message
            $scope.status = 'Unable to create a person: ' + error.message;
        });
    }
    
    $scope.editPerson = function (user) {
        // Defining $http service for creating a person
        $http({
            method: 'PUT',
            url: '/api/users/',
            data: JSON.stringify(user),
            headers: { 'Content-Type': 'application/JSON' }
        }).
        success(function (data) {
            // Showing Success message
        	var editedUser=angular.fromJson(data);
        	for( var i = 0; i < $scope.users.length; i++ ) {
      			if( $scope.users[i].id === editedUser.id ) {
      				$scope.users[i]=editedUser;
      				break;
      			}
        	}
            // Loading people to the $scope

        })
        .error(function (error) {
            // Showing error message
            $scope.status = 'Unable to create a person: ' + error.message;
        });
    }
    
    $scope.showDetails=function(user) {
    	$('tr.user-details').hide();
    	$('tr#user-'+user.id).show('slow');
    }
    
    $scope.showCreate=function() {
    	$('div#user-create').toggle('slow');
    }
});
