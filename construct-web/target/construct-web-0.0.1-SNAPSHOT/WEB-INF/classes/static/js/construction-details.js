window.onload = function handleTabs() {
      $('[data-tab]').on('click', function (e) {
        $(this)
          .addClass('active')
          .siblings('[data-tab]')
          .removeClass('active')
          .siblings('[data-content=' + $(this).data('tab') + ']')
          .addClass('active')
          .siblings('[data-content]')
          .removeClass('active');
        e.preventDefault();
      });
      
};

var app = angular.module('constructionDetailsApp', []);
var usersCtrl=app.controller('constructionDetailsCtrl', function($scope, $http) {
	
	$scope.init = function(constructionId)
	  {
		$scope.taskTimekeeping = {};
	    $http.get('/api/constructions/' +constructionId)
	    .then(function (response) {
	    	$scope.construction = response.data;
	    	 angular.forEach($scope.construction.tasks, function(task, index) {
	 	    	
	 	    	$http.get('/api/tasks/' + task.id + '/timekeeping')
	 		    .then(function (response) {
	 		    	$scope.taskTimekeeping[task.id] = response.data;
	 		    });
	 	    	
	 	    });
	    });
	  };
	  
		$scope.createTask = function() {
			// Defining $http service for creating a person
			$scope.newTask.construction=$scope.construction;
			$http({
				method : 'POST',
				url : '/api/tasks/',
				data : JSON.stringify($scope.newTask),
				headers : {
					'Content-Type' : 'application/JSON'
				}
			}).success(function(data) {
				// Showing Success message
				$scope.construction.tasks.push(angular.fromJson(data));
				// Loading people to the $scope

			}).error(
					function(error) {
						alert('Could not create construction')
						$scope.status = 'Unable to create a construction: '
								+ error.message;
					});
		}
		
		$scope.getTaskTimekeeping=function(taskId) {
			var timekeeping=[];
			var taskKeepingUrl='/api/tasks/' +taskId +'/timekeeping';
			$http.get(taskKeepingUrl)
		    .then(function (response) {
		    	timekeeping = response.data;
		    });
			return timekeeping;
		}
	
	 $scope.showCreateTask = function() {
		$('div#task-create').toggle('slow');
	}
});
