var ConstrApp = angular.module('constrApp', [ 'angularUtils.directives.dirPagination' ]);

var OwnerService = ConstrApp.factory('OwnerService', ['$http', '$q', function($http, $q) {
	return {
			getAll : function() {
				return $http.get('/api/owners').then(function(response) {
					return response.data;
				}, function(errResponse) {
					console.error('Error fetching constructions');
					return $q.reject(errResponse);
				});
			},
			
			create : function(owner) {
				return $http.post('/api/owners', owner).then(
						function(response) {
							return response.data;
						}, function(errResponse) {
							console.error('Error while creating user');
							return $q.reject(errResponse);
						});
			}
		}
	} ]);

/**
 * Constructions service
 */
var ConstrService = ConstrApp.factory('ConstrService', ['$http', '$q', function($http, $q) {
		return {
				getAll : function() {
					return $http.get('/api/constructions').then(function(response) {
						return response.data;
					}, function(errResponse) {
						console.error('Error fetching constructions');
						return $q.reject(errResponse);
					});
				},
				
				create : function(construction) {
					return $http.post('/api/constructions', construction).then(
							function(response) {
								return response.data;
							}, function(errResponse) {
								console.error('Error while creating user');
								return $q.reject(errResponse);
							});
					}
			}
		} ]);


/**
 * Constructions controller
 */
var ConstrCtrl = ConstrApp.controller('ConstrCtrl', ['$scope','ConstrService', 'OwnerService' ,'$http',
		function($scope, ConstrService, OwnerService, $http) {
			var self = this;
			constructions = [];
			owners=[];

			self.loadAllConstructions = function() {
				ConstrService.getAll().then(function(data) {
					self.constructions = data;
				}, function(errResponse) {
					console.error('Could not initialize constructins list');
				});
			};
			
			self.loadAllOwners = function() {
				OwnerService.getAll().then(function(data) {
					self.owners = data;
				}, function(errResponse) {
					console.error('Could not initialize owner list');
				});
			};

			self.createConstruction = function() {
				ConstrService.create(self.newConstruction).then(
						function(reseponse) {
							self.fetchAllConstructions;
							alert('success');
						}, function(errResponse) {
							console.error('Error while creating User.');
					});
			};
			
			self.createOwner = function() {
				OwnerService.create(self.newOwner).then(
						function(reseponse) {
							self.fetchAllConstructions;
							alert('success');
						}, function(errResponse) {
							alert(JSON.stringify(errResponse));
							console.error('Error while creating User.');
					});
			};
			self.loadAllConstructions();
			self.loadAllOwners();

		}
]);
