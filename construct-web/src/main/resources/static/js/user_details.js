var app = angular.module('userDetailsApp', [ 'smart-table' ]);
var usersCtrl = app.controller('userDetailsCtrl', function($scope, $http) {

	$scope.init = function(userId) {
		$scope.taskTimekeeping = {};
		$http.get('/api/users/' + userId).then(function(response) {
			$scope.user = response.data;
		});
		$http.get('/api/payment/paymenttype').then(function(response) {
			$scope.paymentTypes = response.data;
		});
	};
	$scope.UpdateUser = function() {
		// Defining $http service for creating a person
		$http({
			method : 'PUT',
			url : '/api/users/',
			data : JSON.stringify($scope.user),
			headers : {
				'Content-Type' : 'application/JSON'
			}
		}).success(function(data) {
			// Showing Success message
			alert('Success');
			// Loading people to the $scope

		}).error(
				function(error) {
					alert('Error');
				});
	}
});
