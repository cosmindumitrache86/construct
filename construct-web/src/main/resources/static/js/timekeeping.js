var app = angular.module('timekeepingApp', []);
var usersCtrl = app.controller('timekeepingCtrl', function($scope, $http) {
	$scope.timekeeping = {};
	$scope.timekeeping.startTime=new Date();
	$scope.timekeeping.startTime.setHours(08);
	$scope.timekeeping.endTime=new Date();
	$scope.timekeeping.endTime.setHours(17);
	$http.get("/api/constructions").then(function(response) {
		$scope.constructions = response.data;
	});

	$http.get("/api/users").then(function(response) {
		$scope.users = response.data;
	});

	$scope.processForm = function() {
		// Defining $http service for creating a person
		$http({
			method : 'POST',
			url : '/api/timekeeping/',
			data : JSON.stringify($scope.timekeeping),
			headers : {
				'Content-Type' : 'application/JSON'
			}
		}).success(function(data) {
			// Showing Success message
			alert('Success');

		}).error(function(error) {
			// Showing error message
			alert(JSON.stringify(error));
		});
	}
});
