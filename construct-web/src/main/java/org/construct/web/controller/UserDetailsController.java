package org.construct.web.controller;

import org.construct.core.data.User;
import org.construct.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserDetailsController {

	@Autowired
	UserRepository userReposiory;

	@RequestMapping(value="/user/details", method=RequestMethod.GET)
	public String loadConstruction(@RequestParam(value="id", required=false) Long userId, Model model)
	{
		User user= userReposiory.findOne(userId);
		model.addAttribute("user", user);
		return "user_details";
	}
}
