package org.construct.web.controller;

import java.util.Date;

import org.construct.core.data.TimeKeeping;
import org.construct.core.repository.TaskRepository;
import org.construct.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TimeKeepingController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	TaskRepository taskRepository;

	@RequestMapping(value="/timekeeping", method=RequestMethod.GET)
	public String addTimeKeeping(Model model)
	{
		TimeKeeping timeKeeping= new TimeKeeping();
		timeKeeping.setStartTime(new Date());
		model.addAttribute("timekeeping", timeKeeping);
		model.addAttribute("users", userRepository.findAll());
		model.addAttribute("tasks", taskRepository.findAll());

		return "timekeeping";
	}
}
