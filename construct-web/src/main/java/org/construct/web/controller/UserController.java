package org.construct.web.controller;

import org.construct.core.data.User;
import org.construct.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

	@Autowired
	UserRepository userReposiory;

	@RequestMapping(value="/users", method=RequestMethod.GET)
	public String greetingForm(Model model) {
		model.addAttribute("users", userReposiory.findAll());
		return "userlist";
	}

	@RequestMapping(value="/users/create", method=RequestMethod.GET)
	public String createUserGet(Model model) {
		model.addAttribute("user", new User());
		return "usercreate";
	}

	@RequestMapping(value="/user/create", method=RequestMethod.POST)
	public String createUser(@ModelAttribute User user, Model model) {
		User savedUser = userReposiory.save(user);
		model.addAttribute("status", "Success");
		model.addAttribute("user", savedUser);
		return "success";
	}

}
