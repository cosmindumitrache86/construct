package org.construct.web.controller;

import org.construct.core.data.Construction;
import org.construct.core.repository.ConstructionRepository;
import org.construct.core.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ConstructionDetailsController {

	@Autowired
	ConstructionRepository constructionReposiory;

	@Autowired
	TaskRepository taskRepository;

	@RequestMapping(value="/constructions/details", method=RequestMethod.GET)
	public String loadConstruction(@RequestParam(value="id", required=false) Long constructionId, Model model)
	{
		Construction construction = constructionReposiory.findOne(constructionId);
		model.addAttribute("construction", construction);
		return "construction_details";
	}
}
