package org.construct.web.controller;

import org.construct.core.data.Construction;
import org.construct.core.data.Task;
import org.construct.core.repository.ConstructionRepository;
import org.construct.core.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ConstructionController {

	@Autowired
	ConstructionRepository constructionReposiory;

	@Autowired
	TaskRepository taskRepository;

	@RequestMapping(value="/constructions", method=RequestMethod.GET)
	public String displayAll(Model model) {
		model.addAttribute("constructions", constructionReposiory.findAll());
		return "construction_list";
	}

	@RequestMapping(value="/constructions/create", method=RequestMethod.GET)
	public String addConstruction(Model model) {
		model.addAttribute("construction", new Construction());
		return "create_construction";
	}

	@RequestMapping(value="/constructions/create", method=RequestMethod.POST)
	public String createConstruction(@ModelAttribute Construction construction, Model model) {
		constructionReposiory.save(construction);
		model.addAttribute("constructions", constructionReposiory.findAll());
		return "construction_list";
	}

	@RequestMapping(value="/constructions/task/create", method=RequestMethod.GET)
	public String createTask(Model model)
	{
		Iterable<Construction> constructions = constructionReposiory.findAll();
		model.addAttribute("constructions", constructions);
		return "create_task";
	}

	@RequestMapping(value="/constructions/task/create", method=RequestMethod.POST)
	public String doCreateTask(@ModelAttribute Task task, Model model)
	{
		taskRepository.save(task);
		Iterable<Construction> constructions = constructionReposiory.findAll();
		model.addAttribute("constructions", constructions);
		return "construction_list";
	}
}
