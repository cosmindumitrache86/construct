package org.construct.api.controllers;

import org.construct.core.data.User;
import org.construct.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserApiController {

	@Autowired
	UserRepository userReposiory;

	@CrossOrigin
	@RequestMapping(value="/api/users", method=RequestMethod.GET)
	public @ResponseBody Iterable<User> getAllUsers() {
		return userReposiory.findAll();
	}

	@CrossOrigin
	@RequestMapping(value="/api/users/{userId}", method=RequestMethod.GET)
	public @ResponseBody User getById(@PathVariable Long userId) {
		return userReposiory.findOne(userId);
	}

	@CrossOrigin	
	@RequestMapping(value="/api/users/{userId}", method=RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable long userId) {
		userReposiory.delete(userId);
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin
	@RequestMapping(value="/api/users", method=RequestMethod.POST)
	public ResponseEntity<User> createUser(@RequestBody User user) {
		User newUser = userReposiory.save(user);
		return new ResponseEntity<User>(newUser, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/users", method=RequestMethod.PUT)
	public ResponseEntity<User> updateUser(@RequestBody User user) {
		User updatedUser = userReposiory.save(user);
		return new ResponseEntity<User>(updatedUser, HttpStatus.OK);
	}

}
