package org.construct.api.controllers;

import org.construct.core.data.PaymentType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentApiController {

	@CrossOrigin
	@RequestMapping(value="/api/payment/paymenttype", method=RequestMethod.GET)
	public @ResponseBody PaymentType[] getAllUsers() {
		return PaymentType.values();
	}
}
