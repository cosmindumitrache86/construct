package org.construct.api.controllers;

import java.util.Collection;

import org.construct.core.data.Construction;
import org.construct.core.data.Material;
import org.construct.core.data.Owner;
import org.construct.core.data.Task;
import org.construct.core.repository.ConstructionRepository;
import org.construct.core.repository.MaterialRepository;
import org.construct.core.repository.OwnerRepository;
import org.construct.core.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//Test commit test
public class ConstructionApiController {
	@Autowired
	ConstructionRepository constructionReposiory;

	@Autowired
	OwnerRepository ownerRespository;

	@Autowired
	TaskRepository taskReposiory;

	@Autowired
	MaterialRepository materialRespository;

	@CrossOrigin
	@RequestMapping(value="/api/constructions", method=RequestMethod.GET)
	public @ResponseBody Iterable<Construction> getAll() {
		return constructionReposiory.findAll();
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions/{constructionId}", method=RequestMethod.GET)
	public @ResponseBody Construction getById(@PathVariable Long constructionId) {
		return constructionReposiory.findOne(constructionId);
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions/{consId}", method=RequestMethod.DELETE)
	public ResponseEntity<Construction> deleteUser(@PathVariable long consId) {
		constructionReposiory.delete(consId);
		return new ResponseEntity<Construction>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions", method=RequestMethod.POST)
	public ResponseEntity<Construction> createUser(@RequestBody Construction construction) {
		Construction newConstruction = constructionReposiory.save(construction);
		return new ResponseEntity<Construction>(newConstruction, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions", method=RequestMethod.PUT)
	public ResponseEntity<Construction> updateUser(@RequestBody Construction construction) {
		Construction updatedConstruction = constructionReposiory.save(construction);
		return new ResponseEntity<Construction>(updatedConstruction, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/owners", method=RequestMethod.GET)
	public @ResponseBody Iterable<Owner> getOwners() {
		Iterable<Owner> owners = ownerRespository.findAll();
		return owners;
	}

	@CrossOrigin
	@RequestMapping(value="/api/owners", method=RequestMethod.POST)
	public ResponseEntity<Owner> createOwner(@RequestBody Owner owner) {
		Owner newOwner = ownerRespository.save(owner);
		return new ResponseEntity<Owner>(newOwner, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/{constructionId}/materials", method=RequestMethod.GET)
	public @ResponseBody Iterable<Material> getMaterials(@PathVariable Long constructionId) {
		Collection<Material> materials = materialRespository.findByConstructionId(constructionId);
		return materials;
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions/{constructionId}/tasks", method=RequestMethod.GET)
	public @ResponseBody Iterable<Task> getConstructionTasks(@PathVariable Long constructionId) {
		Collection<Task> tasks = taskReposiory.findByConstructionId(constructionId);
		return tasks;
	}
}
