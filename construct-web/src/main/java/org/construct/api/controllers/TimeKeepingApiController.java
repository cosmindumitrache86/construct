package org.construct.api.controllers;

import java.util.Collection;

import org.construct.core.data.TimeKeeping;
import org.construct.core.repository.TimeKeepingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TimeKeepingApiController {

	@Autowired
	private TimeKeepingRepository timeKeepingRepo;

	@CrossOrigin
	@RequestMapping(value="/api/timekeeping", method=RequestMethod.POST)
	public ResponseEntity<TimeKeeping> saveTimekeeping(@RequestBody TimeKeeping timeKeeping) {
		TimeKeeping savedEntity = timeKeepingRepo.save(timeKeeping);
		return new ResponseEntity<TimeKeeping>(savedEntity, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/tasks/{taskId}/timekeeping", method=RequestMethod.GET)
	public Collection<TimeKeeping> getTaskTimekeepings(@PathVariable Long taskId) {
		Collection<TimeKeeping> tasksTimekeepingCollection = timeKeepingRepo.findByTaskId(taskId);
		return tasksTimekeepingCollection;
	}

	@CrossOrigin
	@RequestMapping(value="/api/constructions/{constructionId}/timekeeping", method=RequestMethod.GET)
	public Collection<TimeKeeping> getConstructionsTimekeepings(@PathVariable Long constructionId) {
		Collection<TimeKeeping> constructionTimeKeepingCollection= timeKeepingRepo.findByTask_ConstructionId(constructionId);
		return constructionTimeKeepingCollection;
	}
}
