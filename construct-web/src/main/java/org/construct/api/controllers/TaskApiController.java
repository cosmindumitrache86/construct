package org.construct.api.controllers;

import org.construct.core.data.Task;
import org.construct.core.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//test commi
public class TaskApiController {

	@Autowired
	TaskRepository taskReposiory;

	@CrossOrigin
	@RequestMapping(value="/api/tasks", method=RequestMethod.GET)
	public @ResponseBody Iterable<Task> getAllUsers() {
		return taskReposiory.findAll();
	}

	@CrossOrigin
	@RequestMapping(value="/api/tasks", method=RequestMethod.POST)
	public ResponseEntity<Task> createUser(@RequestBody Task newTask) {
		Task task = taskReposiory.save(newTask);
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}

}
