package org.construct.api.controllers;

import org.construct.core.data.Material;
import org.construct.core.data.UnitMeasure;
import org.construct.core.repository.ConstructionRepository;
import org.construct.core.repository.MaterialRepository;
import org.construct.core.repository.UnitMeasureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MaterialsApiController {

	@Autowired
	ConstructionRepository constructionReposiory;

	@Autowired
	MaterialRepository ownerRespository;

	@Autowired
	UnitMeasureRepository umRespository;

	@CrossOrigin
	@RequestMapping(value="/api/materials", method=RequestMethod.POST)
	public ResponseEntity<Material> createOwner(@RequestBody Material material) {
		Material newMaterial = ownerRespository.save(material);
		return new ResponseEntity<Material>(newMaterial, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value="/api/unitmeasures", method=RequestMethod.GET)
	public ResponseEntity<Iterable<UnitMeasure>> getAllUnitMeasures() {
		Iterable<UnitMeasure> allUnitMeasures = umRespository.findAll();
		return new ResponseEntity<Iterable<UnitMeasure>>(allUnitMeasures, HttpStatus.OK);
	}

}
