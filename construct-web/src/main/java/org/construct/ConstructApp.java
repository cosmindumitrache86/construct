package org.construct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class ConstructApp 
{
	public static void main( String[] args )
	{
		SpringApplication.run(ConstructApp.class, args);
	}
}
