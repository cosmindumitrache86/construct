package org.construct.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.construct.core.validator.TimeKeepingRangeValidator;

@Constraint(validatedBy = TimeKeepingRangeValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TimeKeepingRange {

	String message() default "{validation.timekeeping.range.invalid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
