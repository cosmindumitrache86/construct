package org.construct.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("org.construct.core.repository")
@EnableTransactionManagement
@ComponentScan(basePackages = { "org.construct.core" })
public class CoreConfig {

}
