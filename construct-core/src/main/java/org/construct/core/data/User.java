package org.construct.core.data;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.construct.core.data.embeded.Contact;
import org.construct.core.data.embeded.Payment;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity
@Table(name="const_user")
@EqualsAndHashCode(of={"id"})
public class User {

	@SequenceGenerator(name="User_Gen", sequenceName="User_Seq")
	@Id @GeneratedValue(generator="User_Gen")
	private Long id;

	private String firstName;

	private String lastName;

	private String cnp;

	@Embedded
	private Contact contact;

	@OneToOne(cascade = {CascadeType.ALL})
	private Address address;

	@OneToOne(cascade = {CascadeType.ALL})
	private Payment payment;

}
