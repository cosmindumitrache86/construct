package org.construct.core.data;

import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(of={"id"})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Task {

	@SequenceGenerator(name="Task_Gen", sequenceName="Task_Seq")
	@Id @GeneratedValue(generator="Task_Gen")
	private Long id;

	private String name;

	@Lob
	private String description;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="construction_id", nullable=false) 
	@JsonDeserialize(using=TaskContructionDeserializer.class)
	@JsonBackReference
	private Construction construction;

}

//Check this again.
class TaskContructionDeserializer extends JsonDeserializer<Construction> {

	@Override
	public Construction deserialize(JsonParser parser, DeserializationContext context)
			throws IOException, JsonProcessingException {
		Construction construction = new Construction();
		JsonNode node = parser.getCodec().readTree(parser);
		Long id = node.get("id").longValue();
		construction.setId(id);
		return construction;
	}
}
