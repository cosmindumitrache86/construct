package org.construct.core.data;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"name"})})
@EqualsAndHashCode(of={"id"})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Construction {

	@SequenceGenerator(name="Cons_Gen", sequenceName="Cons_Seq")
	@Id @GeneratedValue(generator="Cons_Gen")
	private Long id;

	private String name;

	private String description;

	@OneToOne(cascade = {CascadeType.ALL})
	private Address address;

	@ManyToOne
	@JoinColumn(name="owener_id")
	private Owner owner;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date deadline;

	@OneToMany(fetch=FetchType.EAGER, mappedBy="construction")
	@JsonManagedReference
	private List<Task> tasks;
}
