package org.construct.core.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Owner {

	@SequenceGenerator(name="Cons_Gen", sequenceName="Cons_Seq")
	@Id @GeneratedValue(generator="Cons_Gen")
	private Long id;

	private String name;

	private String phone;

	@Lob
	private String details;

}
