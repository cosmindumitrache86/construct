package org.construct.core.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of={"id"})
public class Address {

	@SequenceGenerator(name="Addr_Gen", sequenceName="Addr_Seq")
	@Id @GeneratedValue(generator="Addr_Gen")
	private Long id;

	@Lob
	private String address;

	private String district;

	private String zipCode;

	private String city;

	private String country;



}
