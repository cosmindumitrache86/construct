package org.construct.core.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class UnitMeasure {

	@SequenceGenerator(name="UM_Gen", sequenceName="UM_Seq")
	@Id @GeneratedValue(generator="UM_Gen")
	private Long id;

	private String name;
}
