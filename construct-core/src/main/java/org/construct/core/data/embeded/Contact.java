package org.construct.core.data.embeded;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Contact {

	private String mobilePhone;

	private String homePhone;

	private String secondPhone;

	private String email;

}
