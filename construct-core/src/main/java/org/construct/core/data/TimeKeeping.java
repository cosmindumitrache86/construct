package org.construct.core.data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class TimeKeeping {

	@SequenceGenerator(name="TK_Gen", sequenceName="Tk_Seq")
	@Id @GeneratedValue(generator="TK_Gen")
	private Long id;

	@OneToOne
	private User user;

	@ManyToOne
	@JoinColumn(name="task_id", nullable=false)
	private Task task;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endTime;

}
