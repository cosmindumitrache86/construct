package org.construct.core.data.embeded;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.construct.core.data.PaymentType;

import lombok.Data;

@Entity
@Data
public class Payment {

	@SequenceGenerator(name="Salar_Gen", sequenceName="Salar_Seq")
	@Id @GeneratedValue(generator="Salar_Gen")
	private Long id;

	@Enumerated(EnumType.STRING)
	private PaymentType paymentType;

	private Double salary;



}
