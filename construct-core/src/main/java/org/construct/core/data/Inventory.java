package org.construct.core.data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class Inventory {

	@SequenceGenerator(name="Inv_Gen", sequenceName="Inv_Seq")
	@Id @GeneratedValue(generator="Inv_Gen")
	private Long id;

	private String name;

	private String description;

	private Double cost;

	@Temporal(TemporalType.DATE)
	private Date acquisitionDate;


}
