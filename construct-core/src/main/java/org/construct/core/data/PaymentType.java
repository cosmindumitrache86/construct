package org.construct.core.data;

public enum PaymentType {

	HOURLY, DAILY, MONTHLY;

}
