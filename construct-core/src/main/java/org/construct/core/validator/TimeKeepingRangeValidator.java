package org.construct.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.construct.core.annotation.TimeKeepingRange;
import org.construct.core.data.TimeKeeping;

public class TimeKeepingRangeValidator implements ConstraintValidator<TimeKeepingRange, TimeKeeping> {

	@Override
	public void initialize(TimeKeepingRange arg0) {

	}

	@Override
	public boolean isValid(TimeKeeping timeKeeping, ConstraintValidatorContext arg1) {
		if (timeKeeping.getEndTime().before(timeKeeping.getStartTime())) {
			return false;
		}
		return true;
	}

}
