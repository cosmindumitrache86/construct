package org.construct.core.repository;

import java.util.Collection;

import org.construct.core.data.TimeKeeping;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TimeKeepingRepository extends CrudRepository<TimeKeeping, Long> {

	@Transactional
	Collection<TimeKeeping> findByTaskId(long taskId);

	Collection<TimeKeeping> findByTask_ConstructionId(Long constructionId);

}
