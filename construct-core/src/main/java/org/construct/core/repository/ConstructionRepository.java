package org.construct.core.repository;

import javax.transaction.Transactional;

import org.construct.core.data.Construction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ConstructionRepository extends CrudRepository<Construction, Long>{

}
