package org.construct.core.repository;

import javax.transaction.Transactional;

import org.construct.core.data.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface OwnerRepository extends CrudRepository<Owner, Long>{

}
