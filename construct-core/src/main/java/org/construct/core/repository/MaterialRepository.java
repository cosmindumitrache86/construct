package org.construct.core.repository;

import java.util.Collection;

import javax.transaction.Transactional;

import org.construct.core.data.Material;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface MaterialRepository extends CrudRepository<Material, Long>{

	@Transactional
	Collection<Material> findByConstructionId(long constructionId);
}
