package org.construct.core.repository;

import javax.transaction.Transactional;

import org.construct.core.data.UnitMeasure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface UnitMeasureRepository extends CrudRepository<UnitMeasure, Long>{

}
