package org.construct.core.repository;

import java.util.Collection;

import org.construct.core.data.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

	@Transactional
	Collection<Task> findByConstructionId(long constructionId);
}
